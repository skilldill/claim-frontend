import * as React from 'react'
import { render } from 'react-dom'
import { HashRouter, Route, Switch } from 'react-router-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'

import { AuthorizationContainer, CabinetContainer, ModalContainer } from './containers'

import Reducers from './reducers'

const store = createStore(Reducers, applyMiddleware())

const AuthorizationPage = (
    <Provider store={ store }>
        <AuthorizationContainer />
    </Provider>
)

const CabinetPage = (
    <Provider store={ store }>
        <CabinetContainer />
    </Provider>
)

render(
    <HashRouter>
        <div>
            <Route exact path="/" component={ () => AuthorizationPage } />
            <Route path="/cabinet" component={ () => CabinetPage } />
        </div>
    </HashRouter>,
    document.getElementById('claims_root')
)

render(
    <Provider store={ store }>
        <ModalContainer />
    </Provider>,
    document.getElementById('modal')
)
