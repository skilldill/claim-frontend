import { IAction } from '../interfaces'
import * as actions from '../actions'

const intialState = { 
    login:'', 
    password:'',
    attentionLogin:'',
    attentionPassword:''
}

export default function authorization(state:any=intialState, action:IAction ){
    switch(action.type){
        case actions.authorization.SET_LOGIN:
            return { ...state, ...{ login:action.data } }

        case actions.authorization.SET_PASSWORD:
            return { ...state, ...{ password:action.data } }

        case actions.authorization.SET_LOGIN_ATTENTION:
            return { ...state, ...{ attentionLogin:action.data } }

        case actions.authorization.SET_PASSWORD_ATTENTION:
            return { ...state, ...{ attentionPassword:action.data } }

        default:
            return state
    }
}