import { combineReducers } from 'redux'

import authorization from './authorization'
import tableClaim from './tableClaim'
import modal from './modal'

export default combineReducers({ 
    authorization,
    tableClaim,
    modal
})