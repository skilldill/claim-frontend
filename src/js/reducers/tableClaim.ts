import { IAction } from '../interfaces'
import * as actions from '../actions'

const intialState = {
    claims:[
        {
            owner:'Егор Андреевич',
            price:2500,
            law:'44 ФЗ',
            date:'03.02.2019',
            messages:[
                {
                    owner:'Александр Друзь',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                }
            ]
        },
        {
            owner:'Егор Андреевич',
            price:600,
            law:'223 ФЗ',
            date:'03.02.2019',
            messages:[
                {
                    owner:'Егор Андреевич',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                }
            ]
        },
        {
            owner:'Андрей Егорович',
            price:600,
            law:'185 ФЗ',
            date:'03.02.2019',
            messages:[
                {
                    owner:'Дрон Андройдович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                }
            ]
        },
        {
            owner:'Андрей Егорович',
            price:3750,
            law:'615 ПП',
            date:'03.02.2019',
            messages:[
                {
                    owner:'Андрей Егорович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                }
            ]
        },
        {
            owner:'Василий Васильевич',
            price:3750,
            law:'615 ПП',
            date:'03.02.2019',
            messages:[
                {
                    owner:'Василий Васильевич',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Я',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                },
                {
                    owner:'Андрей Кракович',
                    text:'Привет привет привет привет привет привет',
                    date:'15.02.2019'
                }
            ]
        },
    ]
}

const addClaim = (state:any, claim:any) => {
    const updatedClaims = [ ...state.claims ]
    updatedClaims.push(claim)
    return { ...state, ...{ claims:updatedClaims } }
}

const removeClaim = (state:any, index:number) => {
    const updatedClaims = [ ...state.claims ]
    updatedClaims.splice(index, 1)
    return { ...state, ...{ claims:updatedClaims } }
}

const addMessage = (state:any, message:any) => {
    const updatedClaims = [ ...state.claims ]
    const readyMessage:any = {
        owner:message.owner,
        text:message.text,
        date:message.date
    }
    updatedClaims[message.id].messages.push(readyMessage)
    return { ...state, ...{ claims:updatedClaims } }
}

export default function tableClaims(state:any=intialState, action:IAction){
    switch(action.type){
        case actions.tableClaim.ADD_CLAIM:
            return addClaim(state, action.data)

        case actions.tableClaim.REMOVE_CLAIM:
            return removeClaim(state, action.data)

        case actions.tableClaim.ADD_MESSAGE:
            return addMessage(state, action.data)

        default:
            return state
    }
}