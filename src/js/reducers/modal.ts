import { IAction } from '../interfaces'
import * as actions from '../actions'

const initialState = {
    show:false,
    type:'',
    currentChat:{}
}

export default function modal(state:any=initialState, action:IAction){
    switch(action.type){
        case actions.modal.SHOW:
            return { ...state, ...{ show:action.data } }

        case actions.modal.SET_TYPE:
            return { ...state, ...{ type:action.data } }

        case actions.modal.SET_CURRENT_CHAT:
            return { ...state, ...{ currentChat:action.data } }

        default:
            return state
    }
}
