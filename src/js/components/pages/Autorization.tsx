import * as React from 'react'
import { Link } from 'react-router-dom'
import * as actions from '../../actions'

import InputComponent from '../InputComponet'

export default class Authorization extends React.Component<any, {}>{

    setLogin(e:React.ChangeEvent<HTMLInputElement>, dispatch:any){
        dispatch(actions.authorization.setLogin(e.currentTarget.value))
        if(!!e.currentTarget.value)
            dispatch(actions.authorization.setLoginAttention(''))
    }

    setPassword(e:React.ChangeEvent<HTMLInputElement>, dispatch:any){
        dispatch(actions.authorization.setPassword(e.currentTarget.value))
        if(!!e.currentTarget.value)
            dispatch(actions.authorization.setPasswordAttention(''))
    }

    logIn(e:React.MouseEvent<HTMLElement>){
        if(!(!!this.props.login) || !(!!this.props.password)){
            e.preventDefault()

            if(!(!!this.props.login))
                this.props.dispatch(actions.authorization.setLoginAttention('необходимо ввести логин'))

            if(!(!!this.props.password))
                this.props.dispatch(actions.authorization.setPasswordAttention('необходимо ввести пароль'))
        }
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="d-flex justify-content-center align-items-center authorization">
                    <form>
                        <InputComponent 
                            value={ this.props.login }
                            type="text"
                            id="login"
                            dispatch={ this.props.dispatch }
                            change={ this.setLogin }
                            attention={ this.props.attentionLogin }
                            placeholder="Логин" />
                        <InputComponent 
                            value={ this.props.password }
                            type="password"
                            id="password"
                            dispatch={ this.props.dispatch }
                            change={ this.setPassword }
                            attention={ this.props.attentionPassword }
                            placeholder="Пароль" />
                        <Link to="cabinet" onClick={ (e) => { this.logIn(e) } }>
                            <button type="submit" className="btn btn-primary">Войти</button>
                        </Link>
                    </form>
                </div>
            </div>
        )
    }
}