import * as React from 'react'
import * as actions from '../../../actions'

export default class TableClaims extends React.Component<any, {}>{

    addClaim(){
        this.props.dispatch(actions.modal.setType('update'))
        this.props.dispatch(actions.modal.show(true))
    }

    showChat(messages:any, id:number){
        this.props.dispatch(actions.modal.setType('chat'))
        this.props.dispatch(actions.modal.setCurrentChat({ id, messages }))
        this.props.dispatch(actions.modal.show(true))
    }

    redactClaim(){
        this.props.dispatch(actions.modal.setType('redact'))
        this.props.dispatch(actions.modal.show(true))
    }

    renderClaims():React.ReactNode{
        if(this.props.claims){
            return(
                <React.Fragment>
                    {
                        this.props.claims.map((claim:any, id:number) => 
                            <tr className="claims__row" onClick={ () => { this.showChat(claim.messages, id) } }>
                                <th scope="col">{ id + 1 }</th>
                                <th scope="col">{ claim.law }</th>
                                <th scope="col">{ claim.owner }</th>
                                <th scope="col">{ claim.price }</th>
                                <th scope="col">{ claim.date }</th>
                            </tr>
                        )
                    }
                </React.Fragment>
            )
        }
        return null
    }

    render(){
        const claims:React.ReactNode = this.renderClaims()
        return(
            <table className="table table-dark claims">
                <thead>
                    <tr>
                        <th className="claims__head_cell" scope="col">#</th>
                        <th className="claims__head_cell" scope="col">Закон</th>
                        <th className="claims__head_cell" scope="col">Отправитель</th>
                        <th className="claims__head_cell" scope="col">Цена</th>
                        <th className="claims__head_cell" scope="col">Дата создания</th>
                    </tr>
                </thead>
                <tbody>
                    { claims }
                </tbody>
                <tr className="claims__row claims__row-add" onClick={ () => { this.addClaim() } }>
                    <td colSpan={5}>Добавить</td>
                </tr>
            </table>
        )
    }
}