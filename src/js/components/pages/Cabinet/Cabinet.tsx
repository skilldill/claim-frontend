import * as React from 'react'
import { Link } from 'react-router-dom'

import TableClaims from './TableClaims'

export default class Cabinet extends React.Component<any, {}>{

    render(){
        return(
            <div className="container-fluid">
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a className="navbar-brand" href="#">{ this.props.login }</a>
                    <div className="collapse navbar-collapse navbar-end" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link to="/">
                                    <span className="nav-link">Выйти</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                <TableClaims { ...this.props } />
            </div>
        )
    }
}