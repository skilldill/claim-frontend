import * as React from 'react'

import { styleFocused, styleUnFocused } from '../constants'
import { IInputProps, IInputState } from '../interfaces'

export default class InputComponent extends React.Component<IInputProps, IInputState>{
    constructor(props:any){
        super(props)
        this.state = {
            isFocus:false
        }
    }

    focus():void{
        this.setState({ isFocus:true })
    }

    blur(e:React.FocusEvent<HTMLInputElement>):void{
        if(!(!!e.currentTarget.value))
            this.setState({ isFocus:false })
    }

    render(){
        return(
            <div className="form-group">
                <label htmlFor={ this.props.id } style={ this.state.isFocus ? styleFocused : styleUnFocused }>{ this.props.placeholder }</label>
                <input type={ this.props.type } 
                    className="form-control" id={ this.props.id }
                    onFocus={ () => { this.focus() } }
                    onBlur={ (e) => { this.blur(e) } } 
                    onChange={ (e) => { this.props.change ? this.props.change(e, this.props.dispatch) : () => {} } }/>
                <small className="form-text invalid-feedback" style={{ display: !!this.props.attention ? 'block' : 'none' }}>
                    { this.props.attention }
                </small>
            </div>
        )
    }
}