import * as React from 'react'
import * as actions from '../../actions'
import { getCurrentDate } from '../../constants'
import { IChatProps } from '../../interfaces'

export default class Chat extends React.Component<any, IChatProps>{

    constructor(props:any){
        super(props)
        this.state = {
            message:''
        }
    }

    renderMessages():React.ReactNode{
        if(!!this.props.currentChat.messages.length){
            return(
                <React.Fragment>
                    {
                        this.props.currentChat.messages.map((message:any) => 
                            <div className="d-flex">
                                <div className={`chat__message ${ message.owner == 'Я' ? 'chat__message-my' : '' }`}>
                                    <div className="d-flex justify-content-start">
                                        <small>{ message.owner }</small>
                                    </div>
                                    <p className="lead">
                                        { message.text }
                                    </p>
                                    <div className="d-flex justify-content-end">
                                        <small>{ message.date }</small>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </React.Fragment>
            )
        }
        else
            return null
    }

    sendMessage(){
        if(!!this.state.message){
            this.props.dispatch(actions.tableClaim.addMessage({
                id:this.props.currentChat.id,
                owner:'Я',
                date:getCurrentDate(),
                text:this.state.message
            }))
            this.setState({
                message:''
            })
        }
    }

    setMessage(e:React.ChangeEvent<HTMLTextAreaElement>){
        this.setState({
            message:e.currentTarget.value
        })
    }

    removeClaim(){
        this.props.dispatch(actions.modal.show(false))
        this.props.dispatch(actions.tableClaim.removeClaim(this.props.currentChat.id))
    }

    render(){
        const messages:React.ReactNode = this.renderMessages() 

        return(
            <div className="chat">
                <div className="chat__body">
                    { messages }
                </div>
                <div className="chat__field_text">
                    <div className="form-group">
                        <textarea className="form-control" rows={1} onChange={ (e) => { this.setMessage(e) } } value={ this.state.message }>
                        </textarea>
                    </div>
                    <div className="d-flex justify-content-between">
                        <button className="btn btn-danger" onClick={ () => { this.removeClaim() } }>Удалить заявку</button>
                        <button className="btn btn-primary" onClick={ () => { this.sendMessage() } }>Отправить</button>
                    </div>
                </div>
            </div>
        )
    }
}  