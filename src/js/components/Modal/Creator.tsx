import * as React from 'react'
import * as actions from '../../actions'

import InputComponent from '../InputComponet'
import { laws, getCurrentDate } from '../../constants'

export default class Creator extends React.Component<any, { owner:string, law:string, price:number, message:string }>{

    constructor(props:any){
        super(props)
        this.state = {
            owner:'',
            law:laws[0],
            price:0,
            message:''
        }
    }

    setOwner(e:React.ChangeEvent<HTMLInputElement>){
        this.setState({ owner:e.currentTarget.value })
    }

    setLaw(e:React.ChangeEvent<HTMLSelectElement>){
        this.setState({ law:e.currentTarget.value })
    }

    setPrice(e:React.ChangeEvent<HTMLInputElement>){
        this.setState({ price:parseInt(e.currentTarget.value) })
    }

    setComent(e:React.ChangeEvent<HTMLTextAreaElement>){
        this.setState({ message:e.currentTarget.value })
    }

    addClaim(){
        if(!!this.state.owner && !!this.state.price){
            this.props.dispatch(actions.tableClaim.addClaim({
                owner:this.state.owner,
                price:this.state.price,
                law:this.state.law,
                date:getCurrentDate(),
                messages: !!this.state.message ? [{ owner:'Я', date:getCurrentDate(), text:this.state.message }] : []
            }))

            this.setState({
                owner:'',
                price:0,
                message:''
            })
        }
    }

    render(){
        return(
            <form className="creator">
                <div className="form-group">
                    <select className="custom-select my-1 mr-sm-2" onChange={ (e) => { this.setLaw(e) } }>
                        {
                            laws.map((law:string, id:number) => 
                                <option key={id}>{ law }</option>
                            )
                        }
                    </select>
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Цена" 
                        value={ !!this.state.price ? this.state.price : '' } 
                        onChange={ (e) => { this.setPrice(e) } } />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Ф.И.О" 
                        value={ this.state.owner }
                        onChange={ (e) => { this.setOwner(e) } } />
                </div>
                <div className="form-group">
                    <textarea className="form-control" rows={1} placeholder="Коментарий" 
                        value={ this.state.message }
                        onChange={ (e) => { this.setComent(e) } }>
                    </textarea>
                </div>
                <div className="d-flex justify-content-end">
                    <button className="btn btn-primary" onClick={ () => { this.addClaim() } }>Добавить</button>
                </div>
            </form>
        )
    }
}