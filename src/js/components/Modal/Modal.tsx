import * as React from 'react'
import * as actions from '../../actions'

import Chat from './Chat'
import Creator from './Creator'
import Redactor from './Redactor'

export default class Modal extends React.Component<any, {}>{

    close(){
        this.props.dispatch(actions.modal.show(false))
    }

    chooseOption():React.ReactNode{
        console.log(this.props.type)
        console.log(this.props.currentChat)

        switch(this.props.type){
            case 'chat':
                return <Chat { ...this.props } />
            
            case 'update':
                return  <Creator { ...this.props } />

            case 'redact':
                return <Redactor { ...this.props } />

            default:
                return (<div></div>)
        }
    }

    render(){
        const option:React.ReactNode = this.chooseOption()

        return(
            <div className="modal" style={{ display: this.props.show ? 'block' : 'none' }}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{ this.props.type == 'chat' ? 'Заявка' : 'Добавить заявку' }</h5>
                            <button className="close" onClick={ () => { this.close() } }>
                                <span>
                                    &times;
                                </span>
                            </button>
                        </div>
                        <div className="modal-body">
                            { option }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
