import { IAction } from '../interfaces'

class Authorization{
    readonly SET_LOGIN:string = 'authorization.set_login'
    readonly SET_PASSWORD:string = 'authorization.set_password'
    readonly SET_LOGIN_ATTENTION:string = 'authorization.set_login_attention'
    readonly SET_PASSWORD_ATTENTION:string = 'authorization.set_password_attention'

    setLogin = (login:string):IAction => ({ type:this.SET_LOGIN, data:login })
    setPassword = (password:string):IAction => ({ type:this.SET_PASSWORD, data:password })
    setLoginAttention = (attention:string):IAction => ({ type:this.SET_LOGIN_ATTENTION, data:attention })
    setPasswordAttention = (attention:string):IAction => ({ type:this.SET_PASSWORD_ATTENTION, data:attention })
}

class TableClaim{
    readonly ADD_CLAIM:string = 'table.add_claim'
    readonly REMOVE_CLAIM:string = 'table.remove_claim'
    readonly ADD_MESSAGE:string = 'table.add_message'

    addClaim = (claim:any) => ({ type:this.ADD_CLAIM, data:claim })
    removeClaim = (index:any) => ({ type:this.REMOVE_CLAIM, data:index })
    addMessage = (message:any) => ({ type:this.ADD_MESSAGE, data:message })
}

class Modal{
    readonly SHOW:string = 'modal.show'
    readonly SET_TYPE:string = 'modal.set_type'
    readonly SET_CURRENT_CHAT:string = 'modal.set_current_chat'

    show = (isShow:boolean) => ({ type:this.SHOW, data:isShow })
    setType = (type:string) => ({ type:this.SET_TYPE, data:type })
    setCurrentChat = (chat:any) => ({ type:this.SET_CURRENT_CHAT, data:chat })
}

export const authorization = new Authorization()
export const tableClaim = new TableClaim()
export const modal = new Modal()