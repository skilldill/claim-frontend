import * as React from 'react'

export const styleFocused:React.CSSProperties = { top: '-11px', left: '5px', fontSize: '13px', color:'#000', backgroundColor:'#fff' }
export const styleUnFocused:React.CSSProperties = { top: '2px', left: '10px', fontSize: '20px', color:'rgba(0, 0, 0, 0.473)',  backgroundColor:'transparent' }
export const laws = [ '44 ФЗ', '223 ФЗ', '185 ФЗ', '615 ПП' ]

export const getCurrentDate = () => {
    let now:Date = new Date()
    let dd:string = now.getDate().toString()
    let mm:string = (now.getMonth() + 1).toString()
    let yyyy:string = now.getFullYear().toString()

    if (parseInt(dd) < 10) 
        dd = '0' + dd
    
    if (parseInt(mm) < 10) 
        mm = '0' + mm
    
    return `${dd}.${mm}.${yyyy}`
}