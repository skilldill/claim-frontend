import { connect } from 'react-redux'

import Authorization from '../components/pages/Autorization'
import Cabinet from '../components/pages/Cabinet/Cabinet'
import Modal from '../components/Modal/Modal'

export const AuthorizationContainer = connect((state:any) => ({
    login: state.authorization.login,
    password: state.authorization.password,
    attentionLogin: state.authorization.attentionLogin,
    attentionPassword: state.authorization.attentionPassword
}))(Authorization)

export const CabinetContainer = connect((state:any) => ({
    login: state.authorization.login,
    claims: state.tableClaim.claims
}))(Cabinet)

export const ModalContainer = connect((state:any) => ({
    show: state.modal.show,
    type: state.modal.type,
    currentChat: state.modal.currentChat
}))(Modal)