export interface IAction{ type:string, data?:any }
export interface IInputProps{ placeholder:string, type:string, value?:string, change?:any, dispatch?:any ,attention?:string, id?:string }
export interface IInputState{ isFocus:boolean }
export interface IChatProps{ message:string }